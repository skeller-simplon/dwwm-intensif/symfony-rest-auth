<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use App\Form\UserType;

class ApiAuthController extends AbstractController
{
    /**
     * @Route("/api/register", methods="POST")
     */
    public function register(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->submit(json_decode($request->getContent(), true));

        if($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $manager->persist($user);
            $manager->flush();

            return $this->json("", 201);

        }
        return $this->json($form->getErrors(true), 400);
    }

    /**
     * @Route("/api/test", methods="GET")
     */
    public function forTest () {
        return $this->json(["message" => "Coucou ".$this->getUser()->getEmail()]);
    }
}
